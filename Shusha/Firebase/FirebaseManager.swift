//
//  FirebaseManager.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 26/08/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import Foundation
import Firebase
import FirebaseDatabase
import FirebaseAuth

class FirebaseMan {
    static var reference: DatabaseReference!
    static var firebaseData: NSDictionary?
    
    // This function initiates Firebase, if user is not logged in it signs in with
    // credentials
    public static func initFirebase() {
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if(user == nil) {
                Auth.auth().signIn(withEmail: Constants.FIREBASE_EMAIL, password: Constants.FIREBASE_PW) { (usr, error) in
                    if(user != nil) {
                        self.initDataBaseRef()
                        print(usr!.user.email as Any)
                    }
                }
            } else {
                self.initDataBaseRef()
            }
        }
    }
    
    private static func initDataBaseRef() {
        reference = Database.database().reference()
        reference.observeSingleEvent(of: .value, with: { (snapshot) in
            if (snapshot.value != nil) {
                self.firebaseData = snapshot.value as? NSDictionary
                checkIfAppShouldBeKilled()
                checkIfUserIsForbidden()
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    public static func getValueFromFirebase(key: String) -> String {
        if (firebaseData != nil) {
            let value = firebaseData![key] != nil
            if (value) {
                return firebaseData!.value(forKey: key) as! String
            } else {
                return ""
            }
        } else {
            return ""
        }
    }
    
    //If firebase conf set to kill the app, it will be closed after loading...
    private static func checkIfAppShouldBeKilled() {
        let shouldKillExists = self.firebaseData![Constants.FIRE_SHOULD_KILL] != nil
        if (shouldKillExists) {
            let shouldKillApp: Bool = self.firebaseData!.value(forKey: Constants.FIRE_SHOULD_KILL) as! Bool
            if (shouldKillApp == true) {
                exit(0)
            }
        }
    }
    
    private static func checkIfUserIsForbidden() {
        guard let forbiddenData = self.firebaseData![Constants.FIRE_FORBIDDEN_USERS] else {return}
        let data = forbiddenData as! NSDictionary
        for id in data {
            print(id.key)
        }
    }
}
