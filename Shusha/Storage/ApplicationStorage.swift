//
//  ApplicationStorage.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 11/09/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import Foundation
import SwiftyPlistManager

class ApplicationStorage {
    static func addValueToStorage(key: String, value: Any) {
        SwiftyPlistManager.shared.addNew(value, key: key, toPlistWithName: Constants.APP_STORAGE_NAME) { (err) in
            if err == nil {
                print("Value successfully added into plist.")
            }
        }
    }
    
    static func saveValueInStorage(key: String, value: Any) {
        SwiftyPlistManager.shared.save(value, forKey: key, toPlistWithName: Constants.APP_STORAGE_NAME) { (err) in
            if err == nil {
                print("Value successfully saved into plist.")
            }
        }
    }
    
    static func getValueFromStorage(key: String) -> Any {
        var res: Any!
        SwiftyPlistManager.shared.getValue(for: key, fromPlistWithName: Constants.APP_STORAGE_NAME) { (result, err) in
            if err == nil {
                print("The Value is: '\(result ?? "No Value Fetched")'")
                res = result
            }
        }
        return res!
    }
}
