//
//  AppExtensions.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 18/06/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func deletingPrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else { return self }
        return String(self.dropFirst(prefix.count))
    }
    
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        var lcz = NSLocalizedString(self, tableName: tableName, value: self , comment: "")
        if lcz.range(of: "default_") != nil {
            let defStrings = DefaultStrings()
            lcz = defStrings.find(key: self)
        }
        return lcz
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
