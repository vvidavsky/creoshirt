//
//  Analytics.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 13/09/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import Foundation
import Flurry_iOS_SDK
import UIKit

class AppAnalytics {
    private static func getDefaultReportParams() -> Dictionary<String, Any> {
        return [
            "author": UIDevice.current.name,
            "system": UIDevice.current.systemName + " " + UIDevice.current.systemVersion,
            "device": UIDevice.current.model
        ]
    }
    static func logSimpleEvent(name: String) {
        let params = getDefaultReportParams()
        Flurry.logEvent(name, withParameters: params);
    }
    
    static func logLabelAdded(name: String, label: String) {
        var params = getDefaultReportParams()
        params["label"] = label
        Flurry.logEvent(name, withParameters: params);
    }
    
    static func logShareDesignEvent(name: String, color: String, cloth: String) {
        var params = getDefaultReportParams()
        params["cloth"] = cloth
        params["color"] = color
        Flurry.logEvent(name, withParameters: params);
    }
}
