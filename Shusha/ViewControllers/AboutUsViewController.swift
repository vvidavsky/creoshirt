//
//  AboutUsViewController.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 24/08/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController {
    @IBOutlet weak var aboutUsText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViewTexts()
        adjustElementSize()
        AppAnalytics.logSimpleEvent(name: "About us screen entered")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func goToMainScreen(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func initViewTexts() {
        aboutUsText.text = NSLocalizedString("default_about_text".localized(), comment: "")
        aboutUsText.isEditable = false
    }
    
    func adjustElementSize() {
        aboutUsText.translatesAutoresizingMaskIntoConstraints = true
        aboutUsText.sizeToFit()
        aboutUsText.isScrollEnabled = false
    }
}
