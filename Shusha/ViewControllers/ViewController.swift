//
//  ViewController.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 13/06/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import UIKit
import MessageUI

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, MFMailComposeViewControllerDelegate {
    let gesturesHandler = GesturesHandler()
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var clothImage: UIImageView!
    @IBOutlet weak var paintImage: UIImageView!
    
    let genderRing = 3
    let clothTypeRing = 0
    let colorsRing = 1
    let paintsRing = 2
    private var clothTypeRingImages: [UIImage]! = []
    private var colorRingImages: [UIImage]! = []
    private var genderRingImages: [UIImage]! = []
    private var paintsRingImages: [UIImage]! = []
    private var activeClothsCollection = BasicData.womenTshirtsImageNames
    private var activeColorPickerCollection = BasicData.womenTshirtsCircleImageNames
    
    private var pureColor: String!
    private var genderSelected: String?
    private var selectedClothType: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        FirebaseMan.initFirebase()
        InitMainData()
        gesturesHandler.initGesturesForELement(view: self, element: paintImage)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("MEMORY ISSUE")
    }
    
    // Initiating main application data
    func InitMainData() {
        for type in BasicData.womenClothTypes {
            clothTypeRingImages.append(UIImage(named: type)!)
        }
        for color in activeColorPickerCollection {
            colorRingImages.append(UIImage(named: color)!)
        }
        for paint in BasicData.womenPickerPaints {
            paintsRingImages.append(UIImage(named: paint)!)
        }

        pureColor = "white"
        selectedClothType = Constants.TYPE_TSHIRT
        genderSelected = Constants.WOMAN
        UIManager.updateClothImage(imageView: clothImage, type: selectedClothType!, gender: genderSelected!, color: pureColor)
        UIManager.updatePaintImageView(imageView: paintImage, selectedOption: "women_picker_collibri", gender: genderSelected!)
    }
    
    // MARK: PICKER IMPLEMENTATION
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch (component) {
        case genderRing:
            return genderRingImages.count
        case clothTypeRing:
            return clothTypeRingImages.count
        case colorsRing:
            return colorRingImages.count
        case paintsRing:
            return paintsRingImages.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        switch (component) {
        case genderRing:
            return UIManager.getPickerItemImage(collection: genderRingImages, row: row)
        case clothTypeRing:
            return UIManager.getPickerItemImage(collection: clothTypeRingImages, row: row)
        case colorsRing:
            return UIManager.getPickerItemImage(collection: colorRingImages, row: row)
        case paintsRing:
            return UIManager.getPickerItemImage(collection: paintsRingImages, row: row)
        default:
            return UIImageView(image: nil)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch (component) {
        case genderRing:
            genderSelected = BasicData.genders[row]
            if genderSelected == Constants.MAN {
                activeClothsCollection = BasicData.menTshirtsImageNames
                updateColorsComponent(collection: BasicData.menTshirtsImageNames)
            } else {
                activeClothsCollection = BasicData.womenTshirtsImageNames
                updateColorsComponent(collection: BasicData.womenTshirtsImageNames)
            }
            break
        case clothTypeRing:
            if (genderSelected == Constants.WOMAN) {
                let selectedOption = BasicData.womenClothTypes[row]
                if selectedOption == "icon_tshirt" {
                    selectedClothType = Constants.TYPE_TSHIRT
                    activeColorPickerCollection = BasicData.womenTshirtsCircleImageNames
                    activeClothsCollection = BasicData.womenTshirtsImageNames
                }
                if selectedOption == "icon_hoodie" {
                    selectedClothType = Constants.TYPE_HOODIE
                    activeColorPickerCollection = BasicData.hoodieCirclesImageNames
                    activeClothsCollection = BasicData.womenHoodiesImageNames
                }
            } else {
                
            }
            updateColorsComponent(collection: activeColorPickerCollection)
            UIManager.updateClothImage(imageView: clothImage, type: selectedClothType!, gender: genderSelected!, color: pureColor)
            
        case colorsRing:
            let currentSelected = activeColorPickerCollection[row]
            pureColor = currentSelected.deletingPrefix("circle_")
            UIManager.updateClothImage(imageView: clothImage, type: selectedClothType!, gender: genderSelected!, color: pureColor)
            break
        case paintsRing:
            UIManager.updatePaintImageView(imageView: paintImage, selectedOption: BasicData.womenPickerPaints[row], gender: genderSelected!)
            break
        default:
            print("")
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        //        let pickerWidth = pickerView.bounds.size.width
        //        let amountOfComponents = numberOfComponents(in: pickerView)
        //return pickerWidth / CGFloat(amountOfComponents)
        return 100
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    
    // MARK: PICKER VIEW FUNCTIONS
    
    func updateColorsComponent(collection: Array<String>!) {
        colorRingImages.removeAll();
        for color in collection {
            colorRingImages.append(UIImage(named: color)!)
        }
        picker.reloadComponent(colorsRing)
        pureColor = "white"
        picker.selectRow(0, inComponent: colorsRing, animated: true)
        UIManager.updateClothImage(imageView: clothImage, type: selectedClothType!, gender: genderSelected!, color: pureColor)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func emailOrder(_ sender: UIBarButtonItem) {
        ShareHandler.sendMailWithImage(controller: self, view: view, image: clothImage)
        AppAnalytics.logShareDesignEvent(name: "Emailed existing design", color: pureColor, cloth: selectedClothType!)
    }
    
    @IBAction func shareImage(_ sender: UIBarButtonItem) {
        ShareHandler.shareScreenshot(controller: self, view: view, element: clothImage)
        AppAnalytics.logShareDesignEvent(name: "Shared existing design", color: pureColor, cloth: selectedClothType!)
    }
}

