//
//  CreateViewController.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 05/09/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import UIKit
import MessageUI

class CreateViewController: UIViewController, UIPickerViewDelegate, MFMailComposeViewControllerDelegate, UIPickerViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    let fire = FirebaseMan()
    let gesturesHandler = GesturesHandler()
    
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var clothImage: UIImageView!
    @IBOutlet weak var stage: UIView!
    
    var textLabel: UILabel!
    var paintImage: UIImageView!
    var imagesTagCounter = 0
    var labelsTagCounter = 0
    let clothTypeRing = 0
    let colorsRing = 1
    
    private var clothTypeRingImages: [UIImage]! = []
    private var colorRingImages: [UIImage]! = []
    private var genderRingImages: [UIImage]! = []
    private var paintsRingImages: [UIImage]! = []
    private var activeClothsCollection = BasicData.womenTshirtsImageNames
    private var activeColorPickerCollection = BasicData.womenTshirtsCircleImageNames
    
    private var pureColor: String!
    private var genderSelected: String?
    private var selectedClothType: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        InitMainData()
        InitOservers()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        checkIfShouldShowGuide()
    }
    
    // Initiating main application data
    func InitMainData() {
        for type in BasicData.womenClothTypes {
            clothTypeRingImages.append(UIImage(named: type)!)
        }
        for color in activeColorPickerCollection {
            colorRingImages.append(UIImage(named: color)!)
        }
        
        pureColor = "white"
        selectedClothType = Constants.TYPE_TSHIRT
        genderSelected = Constants.WOMAN
        UIManager.updateClothImage(imageView: clothImage, type: selectedClothType!, gender: genderSelected!, color: pureColor)
    }
    
    func InitOservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.addNewLabelToView(_:)), name: Notification.Name(rawValue: "addNewLabelToStage"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.openConfirmRemoveDialog(_:)), name: Notification.Name(rawValue: "removeViewFromStage"), object: nil)
    }
    
    func checkIfShouldShowGuide() {
        let showGuide: Bool! = (ApplicationStorage.getValueFromStorage(key: Constants.SHOW_EDIT_GUIDE) as! Bool)
        if showGuide {
            showEditGuide()
        }
    }
    
    // MARK: Adding label with custom text
    @objc func addNewLabelToView(_ notification: Notification) {
        guard let labelText = notification.userInfo?["text"] as? String else { return }
        guard let labelColor = notification.userInfo?["color"] as? UIColor else {return}
        guard let labelBackgroundColor = notification.userInfo?["background"] as? UIColor else {return}
        guard let labelFontWeight = notification.userInfo?["weight"] as? UIFont else {return}
        guard let hasBackground = notification.userInfo?["hasBackground"] as? Bool else {return}
        guard let radius = notification.userInfo?["radius"] as? CGFloat else {return}
        let tagPrefix = "1"
        let tag = tagPrefix + "\(labelsTagCounter)"
        self.textLabel = UILabel(frame: CGRect(x: 0, y: 0, width: Operations.getElementWidth(element: clothImage) / 3, height: 40))
        self.textLabel.center = Operations.getCenterOfTheScreen(el: clothImage)
        self.textLabel.textAlignment = .center
        self.textLabel.text = labelText
        self.textLabel.textColor = labelColor
        self.textLabel.font = labelFontWeight
        self.textLabel.backgroundColor = labelBackgroundColor
        self.textLabel.tag = Int(tag)!
        self.textLabel.isUserInteractionEnabled = true
        if hasBackground {
            self.textLabel.layer.masksToBounds = true
            self.textLabel.layer.cornerRadius = radius
            self.textLabel.frame.size.width = self.textLabel.intrinsicContentSize.width + 10
            self.textLabel.frame.size.height = 30
        }
        stage.addSubview(self.textLabel)
        gesturesHandler.initGesturesForELement(view: self, element: textLabel)
        gesturesHandler.initLongPressGestureForElement(view: self, element: textLabel)
        labelsTagCounter += 1
        AppAnalytics.logLabelAdded(name: "New label added to stage", label: labelText)
    }
    
    // Adding new image to the view
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        guard let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage else {return}
        let tagPrefix = "2"
        let tag = tagPrefix + "\(imagesTagCounter)"
        self.paintImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 150))
        self.paintImage.center = Operations.getCenterOfTheScreen(el: clothImage)
        self.paintImage.image = image
        self.paintImage.isUserInteractionEnabled = true
        self.paintImage.contentMode = .scaleAspectFit
        self.paintImage.tag = Int(tag)!
        stage.addSubview(self.paintImage)
        gesturesHandler.initGesturesForELement(view: self, element: paintImage)
        gesturesHandler.initLongPressGestureForElement(view: self, element: paintImage)
        imagesTagCounter += 1
        AppAnalytics.logSimpleEvent(name: "Image added to stage")
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func openConfirmRemoveDialog(_ notification: Notification) {
        guard let viewToDelete = notification.userInfo?["view"] as Optional else {return}
        var message: String! = NSLocalizedString("default_remove".localized(), comment: "") + " "
        let tag = String((viewToDelete as AnyObject).tag);
        if tag.hasPrefix("1") {
            message = message + NSLocalizedString("default_remove_label".localized(), comment: "")
        } else if tag.hasPrefix("2") {
            message = message + NSLocalizedString("default_remove_image".localized(), comment: "")
        }
        
        
        let alert = UIAlertController(title: message, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let libButton = UIAlertAction(title: NSLocalizedString("default_remove".localized(), comment: ""), style: UIAlertAction.Style.destructive) { (alert) -> Void in
            (viewToDelete as AnyObject).removeFromSuperview()
            AppAnalytics.logSimpleEvent(name: "Executed: \(message!)")
        }
        let cancelButton = UIAlertAction(title: NSLocalizedString("default_back".localized(), comment: ""), style: UIAlertAction.Style.cancel) { (alert) -> Void in}
        
        alert.addAction(libButton)
        alert.addAction(cancelButton)
        
        // IPAD ONLY - Initiates different type of delete menu for ipads (as floating menu)
        if let popoverPresentationController = alert.popoverPresentationController {
            let theView = viewToDelete as? UIView
            popoverPresentationController.permittedArrowDirections = .down
            popoverPresentationController.sourceView = theView
            popoverPresentationController.sourceRect = CGRect(x: (theView?.bounds.midX)!, y: ((theView?.bounds.midY)! - 30), width: 0, height: 0)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: PICKER IMPLEMENTATION
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch (component) {
        case clothTypeRing:
            return clothTypeRingImages.count
        case colorsRing:
            return colorRingImages.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        switch (component) {
        case clothTypeRing:
            return UIManager.getPickerItemImage(collection: clothTypeRingImages, row: row)
        case colorsRing:
            return UIManager.getPickerItemImage(collection: colorRingImages, row: row)
        default:
            return UIImageView(image: nil)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch (component) {
        case clothTypeRing:
            if (genderSelected == Constants.WOMAN) {
                let selectedOption = BasicData.womenClothTypes[row]
                if selectedOption == "icon_tshirt" {
                    selectedClothType = Constants.TYPE_TSHIRT
                    activeColorPickerCollection = BasicData.womenTshirtsCircleImageNames
                    activeClothsCollection = BasicData.womenTshirtsImageNames
                }
                if selectedOption == "icon_hoodie" {
                    selectedClothType = Constants.TYPE_HOODIE
                    activeColorPickerCollection = BasicData.hoodieCirclesImageNames
                    activeClothsCollection = BasicData.womenHoodiesImageNames
                }
            } else {
                
            }
            updateColorsComponent(collection: activeColorPickerCollection)
            UIManager.updateClothImage(imageView: clothImage, type: selectedClothType!, gender: genderSelected!, color: pureColor)
            
        case colorsRing:
            let currentSelected = activeColorPickerCollection[row]
            pureColor = currentSelected.deletingPrefix("circle_")
            UIManager.updateClothImage(imageView: clothImage, type: selectedClothType!, gender: genderSelected!, color: pureColor)
            break
        default:
            print("")
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return 100
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
    
    func updateColorsComponent(collection: Array<String>!) {
        colorRingImages.removeAll();
        for color in collection {
            colorRingImages.append(UIImage(named: color)!)
        }
        picker.reloadComponent(colorsRing)
        pureColor = "white"
        picker.selectRow(0, inComponent: colorsRing, animated: true)
        UIManager.updateClothImage(imageView: clothImage, type: selectedClothType!, gender: genderSelected!, color: pureColor)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    func showCustomModal(name: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: name)
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func showEditGuide() {
        showCustomModal(name: "EditScreenGuide")
    }
    
    @IBAction func openAddLabelDialog(_ sender: UIBarButtonItem) {
        showCustomModal(name: "AdvancedLabelAlert")
    }
    
    @IBAction func openCustomImagePicker(_ sender: UIBarButtonItem) {
        UIManager.openImagePickerActivity(view: self)
    }
    
    @IBAction func dismissView(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func shareCreatedImage(_ sender: UIBarButtonItem) {
        ShareHandler.shareScreenshot(controller: self, view: view, element: clothImage)
        AppAnalytics.logShareDesignEvent(name: "Created design shared", color: pureColor, cloth: selectedClothType!)
    }
    
    @IBAction func sendEmageViaEmail(_ sender: UIBarButtonItem) {
        ShareHandler.sendMailWithImage(controller: self, view: view, image: clothImage)
        AppAnalytics.logShareDesignEvent(name: "Created design emailed", color: pureColor, cloth: selectedClothType!)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
