//
//  AdvancedLabelViewController.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 15/09/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import UIKit
import ColorSlider

class AdvancedLabelViewController: UIViewController, UITextFieldDelegate {
    private var selectedFontWeight: UIFont?
    private var colorSlider: ColorSlider!
    private var backgroundColorSlider: ColorSlider!
    private var bgrColor: UIColor = UIColor(white: 1, alpha: 0)
    private var textColor: UIColor = .black
    private var hasBackground: Bool = false
    private var borderRadiusNum: CGFloat = 0.0
    
    
    @IBOutlet weak var previewLabel: UILabel!
    @IBOutlet weak var dialogHeader: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var addLabelButton: UIButton!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var labelTextField: UITextField!
    @IBOutlet weak var fontTitle: UILabel!
    @IBOutlet weak var fontWeightPicker: UISegmentedControl!
    @IBOutlet weak var fontColorHeader: UILabel!
    @IBOutlet weak var backgroundColorLabel: UILabel!
    @IBOutlet weak var borderRadiusTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        initViewTexts()
        initColorSlider()
        initBackgroundColorSlider()
        labelTextField.delegate = self
        labelTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    private func initViewTexts() {
        previewLabel.layer.masksToBounds = true
        dialogView.layer.cornerRadius = 10
        previewLabel.text = NSLocalizedString("default_preview".localized(), comment: "")
        dialogHeader.text = NSLocalizedString("default_add_label_ttl".localized(), comment: "")
        cancelButton.setTitle(NSLocalizedString("default_cancel".localized(), comment: ""), for: .normal)
        addLabelButton.setTitle(NSLocalizedString("default_add".localized(), comment: ""), for: .normal)
        labelTextField.placeholder = NSLocalizedString("default_label_input_placeholder".localized(), comment: "")
        fontTitle.text = NSLocalizedString("default_font_weight_ttl".localized(), comment: "")
        fontWeightPicker.setTitle(NSLocalizedString("default_font_weight_regular".localized(), comment: ""), forSegmentAt: 0)
        fontWeightPicker.setTitle(NSLocalizedString("default_font_weight_bold".localized(), comment: ""), forSegmentAt: 1)
        fontColorHeader.text = NSLocalizedString("default_font_color".localized(), comment: "")
        backgroundColorLabel.text = NSLocalizedString("default_label_background".localized(), comment: "")
        borderRadiusTitle.text = NSLocalizedString("default_border_radius".localized(), comment: "")
    }
    
    private func initColorSlider() {
        colorSlider = ColorSlider(orientation: .horizontal, previewSide: .top)
        colorSlider.frame = CGRect(origin: CGPoint(x: 15,y :167), size: CGSize(width: 270, height: 18))
        setAdditionalColorSliderDetails(slider: colorSlider)
        dialogView.addSubview(colorSlider)
        colorSlider.addTarget(self, action: #selector(changedColor(_:)), for: .valueChanged)
    }
    
    private func initBackgroundColorSlider() {
        backgroundColorSlider = ColorSlider(orientation: .horizontal, previewSide: .top)
        backgroundColorSlider.frame = CGRect(origin: CGPoint(x: 15,y :295), size: CGSize(width: 270, height: 18))
        setAdditionalColorSliderDetails(slider: backgroundColorSlider)
        dialogView.addSubview(backgroundColorSlider)
        backgroundColorSlider.addTarget(self, action: #selector(changedBackgroundColor(_:)), for: .valueChanged)
    }
    
    private func setAdditionalColorSliderDetails(slider: ColorSlider) {
        slider.gradientView.layer.borderColor = UIColor.black.cgColor
        slider.gradientView.layer.borderWidth = 1.0
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 20
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        previewLabel.text = " \(String(describing: textField.text!)) "
        previewLabel.textColor = textColor
        addLabelButton.isEnabled = textField.text!.count > 0
    }
    
    @objc func changedColor(_ slider: ColorSlider) {
        previewLabel.textColor = slider.color
        textColor = slider.color
    }
    
    @objc func changedBackgroundColor(_ slider: ColorSlider) {
        previewLabel.backgroundColor = slider.color
        bgrColor = slider.color
        hasBackground = true
    }
    
    // MARK: DIALOG ACTIONS
    @IBAction func dismissDialog(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func callAddNewLabelMethod(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        let weight: UIFont = selectedFontWeight != nil ? selectedFontWeight! : UIFont.systemFont(ofSize: (labelTextField.font?.pointSize)!, weight: .regular)
        let lText: String! = labelTextField.text
        let labelData = [
            "color": textColor,
            "background": bgrColor,
            "hasBackground": hasBackground,
            "text": lText,
            "radius": borderRadiusNum,
            "weight": weight as Any] as [String : Any]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "addNewLabelToStage"), object: nil, userInfo: labelData)
    }
    
    @IBAction func setFontWeight(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 1:
            previewLabel.font = UIFont.boldSystemFont(ofSize: (labelTextField.font?.pointSize)!)
            selectedFontWeight = UIFont.boldSystemFont(ofSize: (labelTextField.font?.pointSize)!)
        default:
            previewLabel.font = UIFont.systemFont(ofSize: (labelTextField.font?.pointSize)!, weight: .regular)
            selectedFontWeight = UIFont.systemFont(ofSize: (labelTextField.font?.pointSize)!, weight: .regular)
        }
    }
    
    @IBAction func setBackgroundBorderRadius(_ sender: UISlider) {
        previewLabel.layer.cornerRadius = CGFloat(sender.value)
        borderRadiusNum = CGFloat(sender.value)
    }
}
