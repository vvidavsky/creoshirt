//
//  EditGuideViewController.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 11/09/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import UIKit

class EditGuideViewController: UIViewController {
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var closeForGoodButton: UIButton!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var guideTitleText: UITextView!
    @IBOutlet weak var imageIconInfo: UILabel!
    @IBOutlet weak var textIconInfo: UILabel!
    @IBOutlet weak var guideMainText: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initControllerTexts()
    }
    
    private func initControllerTexts() {
        dialogView.layer.cornerRadius = 10
        closeButton.setTitle(NSLocalizedString("default_close".localized(), comment: ""), for: .normal)
        closeForGoodButton.setTitle(NSLocalizedString("default_don_show_again".localized(), comment: ""), for: .normal)
        guideTitleText.text = NSLocalizedString("default_guide_head_text".localized(), comment: "")
        imageIconInfo.text = NSLocalizedString("default_guide_image_desc".localized(), comment: "")
        textIconInfo.text = NSLocalizedString("default_guide_label_desc".localized(), comment: "")
        guideMainText.text = NSLocalizedString("default_guide_main_text".localized(), comment: "")
    }
    
    @IBAction func closeDialog(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func closeDialogForGood(_ sender: UIButton) {
        ApplicationStorage.saveValueInStorage(key: Constants.SHOW_EDIT_GUIDE, value: false)
        AppAnalytics.logSimpleEvent(name: "Guide closed for good")
        self.dismiss(animated: true, completion: nil)
    }
}
