//
//  BasicData.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 13/06/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import Foundation

class BasicData {
    public static let genders = [Constants.WOMAN, Constants.MAN]
    public static let womenClothTypes = ["icon_tshirt", "icon_hoodie"]
    public static let circleImageNames = ["circle_white", "circle_black", "circle_blue", "circle_red", "circle_green"]
    public static let menTshirtsCircleImageNames = ["circle_white", "circle_black", "circle_blue", "circle_red", "circle_green"]
    public static let hoodieCirclesImageNames = [
        "circle_white",
        "circle_black",
        "circle_yellow",
        "circle_red",
        "circle_purple",
        "circle_orange",
        "circle_green",
        "circle_blue"
    ]
    public static let womenTshirtsCircleImageNames = [
        "circle_white",
        "circle_black",
        "circle_light_blue",
        "circle_red",
        "circle_pink",
        "circle_yellow",
        "circle_purple",
        "circle_grey",
        "circle_green"
    ]
    public static let menTshirtsImageNames = ["tshirt_men_white", "tshirt_men_black", "tshirt_men_blue", "tshirt_men_red", "tshirt_men_green"]
    public static let womenTshirtsImageNames = [
        "tshirt_women_white",
        "tshirt_women_black",
        "tshirt_women_light_blue",
        "tshirt_women_red",
        "tshirt_women_pink",
        "tshirt_women_yellow",
        "tshirt_women_purple",
        "tshirt_women_grey",
        "tshirt_women_green"
    ]
    public static let womenHoodiesImageNames = [
        "hoodie_women_white",
        "hoodie_women_black",
        "hoodie_women_yellow",
        "hoodie_women_red",
        "hoodie_women_purple",
        "hoodie_women_orange",
        "hoodie_women_green",
        "hoodie_women_blue"
    ]
    public static let womenPickerPaints = [
        "women_picker_collibri",
        "women_picker_friends",
        "women_picker_music",
        "women_picker_cindy",
        "women_picker_doughnut",
        "women_picker_lovecoffe",
        "women_picker_eyes",
        "women_picker_hatgirl",
        "women_picker_hat_lips",
        "women_picker_beauty",
        "women_picker_cats",
        "women_picker_fashion",
        "women_picker_flamingo",
        "women_picker_french_fries",
        "women_picker_gifts",
        "women_picker_moon",
        "women_picker_shoes",
        "women_picker_sexymamma",
        "women_picker_tango",
        "women_picker_heels",
        "women_picker_puppy",
        "women_picker_petite_robe",
        "women_picker_casual_style",
    ]
    public static let womenPaintImageNames = [
        "women_paint_collibri",
        "women_paint_friends",
        "women_paint_music",
        "women_paint_cindy",
        "women_paint_doughnut",
        "women_paint_lovecofee",
        "women_paint_eyes",
        "women_paint_hatgirl",
        "women_paint_hat_lips",
        "women_paint_beauty",
        "women_paint_cats",
        "women_paint_fashion",
        "women_paint_flamingo",
        "women_paint_french_fries",
        "women_paint_gifts",
        "women_paint_moon",
        "women_paint_shoes",
        "women_paint_sexymamma",
        "women_paint_tango",
        "women_paint_heels",
        "women_paint_puppy",
        "women_paint_petite_robe",
        "women_paint_casual_style"
    ]
}
