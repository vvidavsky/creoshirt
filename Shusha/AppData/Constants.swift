//
//  Constants.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 16/06/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import Foundation
class Constants {
    public static let MAN = "men"
    public static let WOMAN = "women"
    public static let KUKI_EMAIL = "pdinapop@gmail.com"
    public static let TYPE_TSHIRT = "tshirt"
    public static let TYPE_HOODIE = "hoodie"
    
    public static let FIREBASE_EMAIL = "vvidavsky@gmail.com"
    public static let FIREBASE_PW = "lespaul"
    public static let FIRE_SHOULD_KILL = "should_kill"
    public static let FIRE_FORBIDDEN_USERS = "forbidden_users"
    
    public static let APP_STORAGE_NAME = "storage"
    public static let SHOW_EDIT_GUIDE = "show_edit_guide"
}
