//
//  DefaultStrings.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 26/08/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import Foundation
class DefaultStrings {
    public let strings: [String: String] = [
        "default_back": "Назад",
        "default_about_text": "Creo Shirt - создай свою собственную футболку Хочешь выделяться и подчеркнуть собственную креативность? У нас есть решение для тебя! \n\nCreo Shirt - это сервис печати футболок с индивидуальным дизайном. Всё просто: выбери модель футболки, цвет, размер и загрузи своё изображение(это может быть подготовленный заранее коллаж, смешная картинка или просто фотография). Если же у тебя нет идей, что изобразить, в приложении уже присутсвуют готовые шаблоны креативных дизайнов. За нами останется малое - распечатать и доставить тебе футболку. \n\nФантазируй! Создавай! Выделяйся!",
        "default_in_dev_message": "Данное приложение находится на стадии разработки, со временем будут появляться новые возможности и варианты рисунков.",
        "default_email_suject": "Вопрос CS",
        "default_add_label_ttl": "Добавить текст",
        "default_cancel": "Отмена",
        "default_add": "Добавить",
        "default_label_input_placeholder": "Внесите текст (обязательно)",
        "default_font_weight_ttl": "Толщина текста",
        "default_font_weight_regular": "Обычный",
        "default_font_weight_bold": "Жирный",
        "default_remove": "Удалить",
        "default_remove_image": "изображение",
        "default_remove_label": "надпись",
        "default_close": "Закрыть",
        "default_don_show_again": "Больше не показывать",
        "default_guide_head_text": "Для создания собственного дизайна используйте следующие ярлыки:",
        "default_guide_image_desc": "чтобы добавить изображение",
        "default_guide_label_desc": "чтобы добавить надпись",
        "default_guide_main_text": "Дизайн может содержать более чем одно изображение и/или одну надпись. Элементы дизайна можно перемещать по экрану, вращать и изменять их величину путем прикосновений. Чтобы удалить какой-либо элемент нажмите на него и удерживайте нажатие до того как появится меню для удаления данного элемента.",
        "default_font_color": "Цвет текста (черный по умолчанию)",
        "default_label_background": "Цвет фона (прозрачный по умолчанию)",
        "default_email_not_registered": "На данном устройстве не обнаружено зарегистрированного адреса электронной почты. Пожалуйста зарегистрируйтесь и повторите попытку.",
        "default_attention": "Внимание!",
        "default_preview": "ПРЕД. ПРОСМОТР",
        "default_border_radius": "Радиус углов фона (0 по умолчанию)"
    ]
    
    public func find(key: String) -> String {
        return self.strings[key]!
    }
}
