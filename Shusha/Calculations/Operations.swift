//
//  Operations.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 03/09/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import Foundation
import UIKit

class Operations {
    static func formatDateAsId() -> String {
        let date = Date()
        let formater = DateFormatter()
        formater.dateFormat = "ddMMyyyyHHmmss"
        return formater.string(from: date)
    }
    
    static func screenWidth() -> CGFloat {
        let screenSize = UIScreen.main.bounds
        return screenSize.width
    }
    
    static func screenHeight() -> CGFloat {
        let screenSize = UIScreen.main.bounds
        return screenSize.height
    }
    
    static func getElementWidth(element: Any) -> CGFloat {
        let size = (element as AnyObject).bounds
        return size!.width
    }
    
    static func getElementHeight(element: Any) -> CGFloat {
        let size = (element as AnyObject).bounds
        return size!.height
    }
    
    static func getCenterOfTheScreen(el: Any) -> CGPoint {
        let stagewidth = getElementWidth(element: el)
        let stageHeight = getElementHeight(element: el)
        return CGPoint(x: stagewidth / 2, y: stageHeight / 2)
    }
}
