//
//  UiOperations.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 22/06/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//
import UIKit
import Foundation

class UIManager {
    public static func InitPaintImagePosition(paintImage: UIImageView, clothImage: UIImageView) {
        let screenSize: CGRect = UIScreen.main.bounds
        let shirtImage: CGRect = clothImage.frame
        paintImage.frame = CGRect(x: 0, y: 0, width: shirtImage.width / 3.5, height: shirtImage.height / 3)
        paintImage.frame.origin = CGPoint(x: screenSize.width / 2 - paintImage.frame.width / 2, y: shirtImage.height * 0.30)
    }
    
    public static func updateClothImage(imageView: UIImageView, type:String, gender: String, color: String) {
        let imageName = "\(type)_\(gender)_\(color)"
        let image = UIImage(named: imageName)
        UIView.transition(with: imageView,
                          duration: 0.3,
                          options: .transitionCrossDissolve,
                          animations: {imageView.image = image},
                          completion: nil)
    }
    
    public static func updatePaintImageView(imageView: UIImageView, selectedOption: String, gender: String) {
        let imagePostFix = selectedOption.deletingPrefix("\(gender)_picker_")
        let imageName = "\(gender)_paint_\(imagePostFix)"
        let image = UIImage(named: imageName)
        imageView.image = image
    }
    
    public static func getPickerItemImage (collection: [UIImage], row: Int) -> UIImageView {
        let image = collection[row]
        let imageView = UIImageView(image: image)
        imageView.frame.size = CGSize(width: 25, height: 25)
        return imageView
    }
    
    public static func openImagePickerActivity(view: UIViewController) {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = (view as! UIImagePickerControllerDelegate & UINavigationControllerDelegate)
            imagePicker.sourceType = .photoLibrary;
            //imagePicker.allowsEditing = false
            view.present(imagePicker, animated: true, completion: nil)
        }
    }
}
