//
//  Dialogs.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 02/10/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import UIKit

class Dialogs {
    static func ShowSimpleNoteDialog(title: String, message: String, view: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: NSLocalizedString("default_close".localized(), comment: ""), style: .default, handler: nil)
        alert.addAction(action)
        view.present(alert, animated: true, completion: nil)
    }
}

