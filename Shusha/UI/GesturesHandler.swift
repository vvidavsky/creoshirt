//
//  GesturesHandler.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 20/08/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//
import Foundation
import UIKit

class GesturesHandler {
    var initialCenter = CGPoint()
    
    func initGesturesForELement(view: UIViewController, element: Any) {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(moveElementOnTheScreen))
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(scaleElement))
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(rotateElement))
        (element as AnyObject).addGestureRecognizer(pan)
        (element as AnyObject).addGestureRecognizer(pinch)
        (element as AnyObject).addGestureRecognizer(rotate)
    }
    
    func initLongPressGestureForElement(view: UIViewController, element: Any) {
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(openMenuOnLongPress))
        longPress.minimumPressDuration = 1
        (element as AnyObject).addGestureRecognizer(longPress)
    }

    // Method that draggs/relocates the view on the screen
    @objc func moveElementOnTheScreen(sender: UIPanGestureRecognizer) {
        guard sender.view != nil else {return}
        let piece = sender.view!
        let translation = sender.translation(in: piece.superview)
        if sender.state == .began {
            self.initialCenter = piece.center
        }
        
        if sender.state != .cancelled {
            let newCenter = CGPoint(x: initialCenter.x + translation.x, y: initialCenter.y + translation.y)
            piece.center = newCenter
        } else {
            // On cancellation, return the piece to its original location.
            piece.center = initialCenter
        }
    }
    
    // Method that scales the view
    @objc func scaleElement(sender: UIPinchGestureRecognizer) {
        guard sender.view != nil else { return }
        if sender.state == .began || sender.state == .changed {
            sender.view?.transform = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale))!
            sender.scale = 1.0
        }
    }
    
    // Method that rotates the view
    @objc func rotateElement(sender: UIRotationGestureRecognizer) {
        guard sender.view != nil else { return }
        if sender.state == .began || sender.state == .changed {
            sender.view?.transform = sender.view!.transform.rotated(by: sender.rotation)
            sender.rotation = 0
        }
        
    }
    
    @objc func openMenuOnLongPress(sender: UILongPressGestureRecognizer) {
        let data = ["view": sender.view]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "removeViewFromStage"), object: nil, userInfo: data as Any as? [AnyHashable : Any])
    }
}

