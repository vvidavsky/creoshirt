//
//  SharesOperator.swift
//  Shusha
//
//  Created by Vlad Vidavsky on 08/09/2018.
//  Copyright © 2018 Vlad Vidavsky. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class ShareHandler {
    
    static func captureScreen(view: UIView, element: UIImageView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(element.bounds.size, false, UIScreen.main.scale)
        guard let context = UIGraphicsGetCurrentContext() else { return .none }
        view.layer.render(in: context)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    static func shareScreenshot(controller: UIViewController, view: UIView, element: UIImageView) {
        let image = captureScreen(view: view, element: element)
        let imageToShare = [ image! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = controller.view // so that iPads won't crash
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook, UIActivity.ActivityType.mail, UIActivity.ActivityType.message, UIActivity.ActivityType.addToReadingList ]
        controller.present(activityViewController, animated: true, completion: nil)
    }
    
    static func sendMailWithImage(controller: UIViewController, view: UIView, image: UIImageView) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = (controller as! MFMailComposeViewControllerDelegate);
            mail.setToRecipients([FirebaseMan.getValueFromFirebase(key: "email")])
            mail.setSubject(NSLocalizedString("default_email_suject".localized(), comment: "") + Operations.formatDateAsId())
            let imageData: NSData = ShareHandler.captureScreen(view: view, element: image)!.pngData()! as NSData
            mail.addAttachmentData(imageData as Data, mimeType: "image/png", fileName: "imageName.png")
            controller.present(mail, animated: true, completion: nil)
        } else {
            Dialogs.ShowSimpleNoteDialog(title: NSLocalizedString("default_attention".localized(), comment: ""),
                                         message: NSLocalizedString("default_email_not_registered".localized(), comment: ""),
                                         view: controller)
            AppAnalytics.logSimpleEvent(name: "Email is not registered on device")
        }
    }
}
